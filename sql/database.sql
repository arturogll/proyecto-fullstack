CREATE DATABASE todoList;


# CREARDO TABLAS 

CREATE TABLE tarea(
idTarea INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
descripcion VARCHAR(136),
fechaInicio DATE,
fechaLimite DATE,
importancia INTEGER,
completada BOOLEAN
);

CREATE TABLE persona(
	idPersona INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	nombre VARCHAR(50),
	correo VARCHAR(50)
);

CREATE TABLE tareaPersona(
	idTareaPersona INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	idPersona INTEGER,
	idTarea INTEGER,
	FOREIGN KEY (idPersona) REFERENCES persona(idPersona),
	FOREIGN KEY (idTarea) REFERENCES tarea(idTarea)
	# FOREIGN KEY (campo que sera llave foranea) REFERENCES tabla(campo)
);
